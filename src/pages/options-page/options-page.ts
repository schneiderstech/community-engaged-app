import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Variables } from '../../providers/variables';
import { AuthService } from '../../providers/auth-service';

import { LoginPage } from '../../pages/login-page/login-page';
import { ProfilePage } from '../../pages/profile-page/profile-page';

@Component({
    selector: 'page-options-page',
    templateUrl: 'options-page.html'
})
export class OptionsPage {
  
    pageContent: any;
    listOptions: Array<{icon: string, title: any, component: any}>;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public authService: AuthService,
        public variables: Variables) {
            this.pageContent = this.variables.getPageOptions();

        this.listOptions = [
            { icon: 'contact', title: this.variables.getPageProfile(), component: ProfilePage },
            // { icon: 'bowtie', title: this.variables.getPageLocalEvents(), component: ProfilePage },
            // { icon: 'phone-portrait', title: this.variables.getPageContactNumber(), component: ProfilePage },
            
        ];
            
        }

    ionViewDidLoad() {
        console.log('ionViewDidLoad OptionsPagePage');
    }

    itemSelected(item) {
        this.navCtrl.push(item);
    }

    logout() {
        this.authService.logoutUser();
        this.navCtrl.setRoot(LoginPage);
    }  
    

}
