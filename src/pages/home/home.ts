import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { LoginPage } from '../../pages/login-page/login-page';
import { OptionsPage } from '../../pages/options-page/options-page';
import { CreateAccountStep2Page } from '../create-account-step-2/create-account-step-2';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    loading: any;
    
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public authService: AuthService
    ) {}

    ionViewDidLoad() {
        // this.showLoader();

        //Check if already authenticated
        this.authService.checkAuthentication().then((res) => {

        }, (err) => {
            this.navCtrl.setRoot(LoginPage);
        });
    }

    openOptions(){
        this.navCtrl.push(OptionsPage);
    }

    goToLocationPage() {
        this.navCtrl.setRoot(CreateAccountStep2Page);
    }
 }
