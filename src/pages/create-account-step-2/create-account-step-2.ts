import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from 'ionic-native';
import { Storage } from '@ionic/storage';

//providers
import { Variables } from '../../providers/variables';
// import { CountryService } from '../../providers/country-service';
import { AuthService } from '../../providers/auth-service';
import { UserService } from '../../providers/user-service';
import { ConnectivityService } from '../../providers/connectivity-service';
import { GoogleAPIKey } from '../../providers/google-api-key';

// pages
import { CreateAccountStep3Page } from '../create-account-step-3/create-account-step-3'
import { HomePage } from '../../pages/home/home';


declare var google;

@Component({
    selector: 'page-create-account-step-2',
    templateUrl: 'create-account-step-2.html',
    // providers: [CountryService]
})
export class CreateAccountStep2Page {
    
    @ViewChild('map') mapElement: ElementRef;

    loading: any;
    formCreateAccountStep2: FormGroup;
    map: any;
    mapInitialised: boolean = false;
    apiKey: any;
    pageContent: any;
    countries: any;
    latLng: any;
    mapOptions: any;
    myEmail: any;
    userCoordinates: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public variables: Variables ,
        public authService: AuthService,
        public userService: UserService,
        private formBuilder: FormBuilder,        
        // public countryService: CountryService,
        public connectivityService: ConnectivityService,
        private googleAPIKey: GoogleAPIKey,
        public storage: Storage
    ) {
        this.pageContent = this.variables.getPageCreateProfileStep2();
        this.apiKey = this.googleAPIKey.getGoogleApiKeys();
        this.formCreateAccountStep2 = this.formBuilder.group({
            user_location: ['', Validators.compose([Validators.required])],
        });
        // this.listCountries();
    }

    // listCountries(){

    //     this.countryService.load()
    //         .then(data => {
    //             this.countries = data;
    //         });

    // }
    
    selectedCountry(countryName) {
        this.navCtrl.push(CreateAccountStep3Page);
    }

    ionViewDidLoad() {
        this.showLoader(this.pageContent.showLoaderLoadingMap);
        this.loadGoogleMaps();
    }

    loadGoogleMaps(){
    
        this.addConnectivityListeners();
    
        if(typeof google == "undefined" || typeof google.maps == "undefined") {

            this.disableMap();
            
            if(this.connectivityService.isOnline()){
                //Load the SDK
                window['mapInit'] = () => {
                    this.initMap();
                    this.enableMap();
                }
            
                let script = document.createElement("script");
                script.id = "googleMaps";
            
                if(this.apiKey){
                    script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey.maps + '&callback=mapInit';
                } else {
                    script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';       
                }
            
                document.body.appendChild(script);  
        
            } else {
                // TODO
                // Create alert to refresh connectivity status when connection is not stable...
                this.showLoader(this.pageContent.showLoaderConnectivityIsOffline);
            } 
        }else {
        
            if(this.connectivityService.isOnline()) {
                this.initMap();
                this.enableMap();
            }
            else {
                this.loading.dismiss();
                this.disableMap();
            }        
        }    
    }
 
    initMap(){
    
        this.mapInitialised = true;
    
        Geolocation.getCurrentPosition().then((position) => {

            this.userCoordinates = position;

            this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            this.mapOptions = {
                center: this.latLng,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

            this.addMarker();
            // this.reverseGeocode(this.latLng, this.mapOptions);
        });    
    }
 
    disableMap() {
    }
    
    enableMap(){
    }
    
    addConnectivityListeners() {
    
        let onOnline = () => {
    
            setTimeout(() => {
                if(typeof google == "undefined" || typeof google.maps == "undefined"){
        
                    this.loadGoogleMaps();
        
                } else {
        
                    if(!this.mapInitialised){
                        this.initMap();
                    }
        
                    this.enableMap();

                }
            }, 2000);
        
        };
    
        let onOffline = () => {
            this.disableMap();
            this.loading.dismiss();
        };
    
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    
    }

    addMarker() {
    
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter()
        });
        let content = '<h4>' + this.pageContent.marker +'</h4>';

        this.addInfoWindow(marker, content);    

        // once the map has completely loaded, remove function showLoader
        this.loading.dismiss();
                    
    }

    addInfoWindow(marker, content) {
    
        let infoWindow = new google.maps.InfoWindow({
            content: content
        });
        
        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });
    
    }  
  
    saveUserLocation() {
        this.showLoader(this.pageContent.showLoaderSavingLocation);
        
        // this returns a promise -> get user's email
        this.authService.getUserCredentials().then(userEmail => {


            let details = {
                user_email: userEmail,
                user_location: {
                    latitude: this.userCoordinates.coords.latitude,
                    longitude: this.userCoordinates.coords.longitude
                }
            };

            // save location into the database
            this.userService.addUserLocation(details).then((result) => {
                this.loading.dismiss();
                this.navCtrl.setRoot(HomePage);

            }, (err) => {
                this.loading.dismiss();
            });

            // this.reverseGeocode(this.latLng, this.mapOptions);

        });        
    
    }

    // get location of the user and output the address like: Melbourne, Australia
    // TODO make reverseGeocode a service provider
    reverseGeocode(latLng, mapOptions) {
        let userLocation;
        let geocoder = new google.maps.Geocoder; 

        // asynchronous method
        geocoder.geocode({'location': latLng}, function(results, status) {
                
            if (status === 'OK') {
                if (results[1]) {
                    
                    userLocation = results[1].formatted_address;                    
                    
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }

    showLoader(content){
    
        this.loading = this.loadingCtrl.create({
            content: content
        });
    
        this.loading.present();
    
    }     
 
}    
