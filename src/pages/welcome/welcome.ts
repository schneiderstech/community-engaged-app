import { Component } from '@angular/core';
// import { Storage } from '@ionic/storage';
import { AuthService } from '../../providers/auth-service';
import { Variables } from '../../providers/variables';
// import { UserLocation} from '../../providers/geolocation';
import { NavController, LoadingController } from 'ionic-angular';
import { LoginPage } from '../../pages/login-page/login-page';
import { HomePage } from '../../pages/home/home';


@Component({
    templateUrl: 'welcome.html'
})

export class Welcome {
    loading: any;
    
    constructor(
        public authService: AuthService,
        // public storage: Storage, 
        public variables: Variables,
        // public userLocation: UserLocation, 
        public navCtrl: NavController,
        public loadingCtrl: LoadingController
        ) {

        // this.checkUserFirstAccess();

    }

    mySlideOptions = {
        initialSlide: 0,
        loop: false,
        pager: true,
    };

    slides = [
        {
            title: `Welcome!`,
            description: `We are here to make you feel at home.`,
            image: "assets/img/logo.svg",
        },
        {
            title: `Get Involved!!`,
            description: `Would you like to be part of your community? <br> Here you can!`,
            image: "assets/img/intro-1.svg",
        }
    ];

    ionViewDidLoad() {

        this.showLoader();
        //Check if already authenticated
        this.authService.checkAuthentication().then((res) => {
            console.log("Already authorized");
            this.loading.dismiss();
            this.navCtrl.setRoot(HomePage);
        }, (err) => {
            this.loading.dismiss();
        });
    }    
    showLoader(){
    
        this.loading = this.loadingCtrl.create({
            content: 'Loading...'
        });

        this.loading.present();
    
    }    
    

    getUserDetails() {

        // console.log('latitude', this.userLocation.getUserLatitude());
        // console.log('longitude', this.userLocation.getUserLongitude());

        // return [
        //     this.userLocation.getUserLatitude(),
        //     this.userLocation.getUserLongitude()
        // ]


        // this.userLocation.getUserLatitude(),
        // this.userLocation.getUserLongitude()



        // this.storage.set(this.variables.getUserFirstAccess(), true);
        // this.navCtrl.setRoot(LoginPage);

    }


}