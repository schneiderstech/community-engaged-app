import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//providers
import { Variables } from '../../providers/variables';
import { GlobalValidator } from '../../providers/global-validator';
import { AuthService } from '../../providers/auth-service';
// pages
import { LoginPage } from '../../pages/login-page/login-page';
import { CreateAccountStep2Page } from '../../pages/create-account-step-2/create-account-step-2';

@Component({
    selector: 'page-create-account-step-1',
    templateUrl: 'create-account-step-1.html'
})
export class CreateAccountStep1Page {
    
    formCreateAccountStep1: FormGroup;
    profilePicture: any = "assets/img/avatar.svg";
    loading: any;
    pageContent: any;
    
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        private formBuilder: FormBuilder,
        public variables: Variables ,
        public loadingCtrl: LoadingController,
        public globalValidator: GlobalValidator,
        public authService: AuthService
    ) {
        this.pageContent = this.variables.getPageCreateProfileStep1();
        this.formCreateAccountStep1 = this.formBuilder.group({
            user_email: ['', Validators.compose([Validators.pattern(this.globalValidator.getEmailValidation()), Validators.required])],
            user_password: ['', Validators.compose([Validators.pattern(this.globalValidator.getPasswordValidation()), Validators.required])]
        });
        
    }

    createAccountStep1(user_email: string, user_password: string) {

        this.showLoader(this.pageContent.showLoaderLoading);

        if(this.formCreateAccountStep1.valid){

            let details = {
                user_email: this.formCreateAccountStep1.value.user_email,
                user_password: this.formCreateAccountStep1.value.user_password
            };

            this.authService.createAccount(details).then((result) => {
                this.loading.dismiss();
                this.navCtrl.setRoot(CreateAccountStep2Page);

            }, (err) => {
                this.loading.dismiss();
            });        
        }         
    }

    loginPage() {
        this.navCtrl.push(LoginPage);
    }
    
    showLoader(content){
    
        this.loading = this.loadingCtrl.create({
            content: content
        });
    
        this.loading.present();
    
    }    
}
