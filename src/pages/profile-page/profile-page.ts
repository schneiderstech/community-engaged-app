import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Variables } from '../../providers/variables';
import { HomePage } from '../../pages/home/home';

 
 @Component({
    selector: 'page-profile-page',
    templateUrl: 'profile-page.html'
})
export class ProfilePage {

    pageContent: any;
    private imageSrc: string;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public variables: Variables) {
            this.pageContent = this.variables.getPageProfile();
        }

    ionViewDidLoad() {

    }

    homePage() {
        this.navCtrl.setRoot(HomePage);
    }

    private openGallery(): void {
        let cameraOptions = {
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: Camera.DestinationType.FILE_URI,      
            quality: 100,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: Camera.EncodingType.JPEG,      
            correctOrientation: true
        }

        Camera.getPicture(cameraOptions)
            .then(file_uri => this.imageSrc = file_uri, 
            err => console.log(err)); 
    }


}
