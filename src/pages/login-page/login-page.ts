import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalValidator } from '../../providers/global-validator';
import { CreateAccountStep1Page } from '../../pages/create-account-step-1/create-account-step-1';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service';

@Component({
    selector: 'page-login-page',
    templateUrl: 'login-page.html'
})
export class LoginPage {

    formLoginPage: FormGroup;
    loginPageLogo: any = "assets/img/avatar.svg";
    loading: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        private formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public globalValidator: GlobalValidator,
        public authService: AuthService
        
    ) {
        this.formLoginPage = this.formBuilder.group({
            user_email: ['', Validators.compose([Validators.pattern(this.globalValidator.getEmailValidation()), Validators.required])],
            user_password: ['', Validators.compose([Validators.pattern(this.globalValidator.getPasswordValidation()), Validators.required])]
        });
        
    }

    loginPage(user_email: string, user_password: string) {
        
        this.showLoader();

        if(this.formLoginPage.valid){
            
            let credentials = {
                user_email: this.formLoginPage.value.user_email,
                user_password: this.formLoginPage.value.user_password
            };

            this.authService.loginUser(credentials).then((result) => {
                this.loading.dismiss();
                this.navCtrl.setRoot(HomePage);
            }, (err) => {
                this.loading.dismiss();
            });
        } 
    }

    createAccountStep1() {
        this.navCtrl.push(CreateAccountStep1Page);
    }

    showLoader(){
        this.loading = this.loadingCtrl.create({
            content: 'Authenticating...'
        });

        this.loading.present();
    }    


}
