import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Variables } from '../providers/variables';
import { AuthService } from '../providers/auth-service';

// this will be our first page, but for now we go straight to create-account for quickly development
import { HomePage } from '../pages/home/home';

//  to build menu list we import all pages that we want
import { CreateAccountStep2Page } from '../pages/create-account-step-2/create-account-step-2';



@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    sideMenuLeft: Array<{icon: string, title: string, component: any}>;
    
    // use this one
    rootPage = HomePage;

    // For development only
    // rootPage = CreateAccountStep2Page;

    constructor(
            platform: Platform,
            public authService: AuthService,
            public variables: Variables,
        
        ) {

        this.sideMenuLeft = [
            { icon: 'paper', title: this.variables.getPageNews(), component: CreateAccountStep2Page },
            { icon: 'bowtie', title: this.variables.getPageLocalEvents(), component: CreateAccountStep2Page },
            { icon: 'phone-portrait', title: this.variables.getPageContactNumber(), component: CreateAccountStep2Page },
            
        ];

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();

        });
    }

    openPageLeft(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

}
