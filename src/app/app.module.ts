import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';
// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';

// providers
import { Variables } from '../providers/variables';
import { GlobalValidator } from '../providers/global-validator';
import { AuthService } from '../providers/auth-service';
import { UserService } from '../providers/user-service';
import { ConnectivityService } from '../providers/connectivity-service';
import { GoogleAPIKey } from '../providers/google-api-key';
import { firebaseConfig } from '../providers/firebaseConfig';

// Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login-page/login-page';
import { CreateAccountStep1Page } from '../pages/create-account-step-1/create-account-step-1';
import { CreateAccountStep2Page } from '../pages/create-account-step-2/create-account-step-2';
import { ProfilePage } from '../pages/profile-page/profile-page';
import { Welcome } from '../pages/welcome/welcome';
import { OptionsPage } from '../pages/options-page/options-page';


@NgModule({
    declarations: [
        MyApp,
        Welcome,
        HomePage,
        LoginPage,
        CreateAccountStep1Page,
        CreateAccountStep2Page,
        ProfilePage,
        OptionsPage
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        AngularFireModule.initializeApp(firebaseConfig)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        Welcome,
        HomePage,
        LoginPage,
        CreateAccountStep1Page,
        CreateAccountStep2Page,
        ProfilePage,
        OptionsPage
    ],
    providers: [
        {
            provide: ErrorHandler, 
            useClass: IonicErrorHandler
        }, 
        Storage, 
        Variables, 
        GlobalValidator,
        AuthService,
        UserService,
        ConnectivityService,
        GoogleAPIKey
    ]
})
export class AppModule {}
