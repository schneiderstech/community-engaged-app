import { Injectable } from '@angular/core';

@Injectable()
export class Variables {

    pageNews = 'News';
    pageLocalEvents = 'Local Events';
    pageContactNumber = 'Contact Numbers';
    pageProfile = {
        title: 'Profile',
        buttonGetImage: 'Upload Image'
    };
    pageOptions = {
        title: 'Options',
        logout: 'Logout'
    }
    pageCreateProfileStep1 = {
        formTitle: 'Sign in to continue',
        formUserEmail: 'Email',
        formUserPassword: 'Password',
        formButtonBack: 'Back',
        formButtonNext: 'Next',
        showLoaderLoading: 'Loading...'
    }
    pageCreateProfileStep2 = {
        title: 'Select Location',
        marker: 'Your Location',
        buttonSaveUserLocation: 'Save Location',
        showLoaderLoadingMap: 'Loading Map...',
        showLoaderSavingLocation: 'Saving Location...',
        showLoaderConnectivityIsOffline: 'No connectivity.'
    }
    
    constructor() {}

    setPageCreateProfileStep2(value) {
        this.pageCreateProfileStep2 = value;
    }
    getPageCreateProfileStep2(){
        return this.pageCreateProfileStep2;
    }

    setPageCreateProfileStep1(value) {
        this.pageCreateProfileStep1 = value;
    }
    getPageCreateProfileStep1(){
        return this.pageCreateProfileStep1;
    }

    setPageOptions(value) {
        this.pageOptions = value;
    }
    getPageOptions(){
        return this.pageOptions;
    }

    setPageProfile(value) {
        this.pageProfile = value;
    }
    getPageProfile(){
        return this.pageProfile;
    }

    setPageContactNumber(value) {
        this.pageContactNumber = value;
    }
    getPageContactNumber(){
        return this.pageContactNumber;
    }

    setPageLocalEvents(value) {
        this.pageLocalEvents = value;
    }
    getPageLocalEvents(){
        return this.pageLocalEvents;
    }
    
    setPageNews(value) {
        this.pageNews = value;
    }
    getPageNews(){
        return this.pageNews;
    }

}
