import { Injectable } from '@angular/core';

@Injectable()
export class GoogleAPIKey {

    googleApiKeys = {
        maps: 'AIzaSyDs734slfACHk2Tav1xuTaNYM20435lweE'        
    }
    constructor() {}

    setGoogleApiKeys(value) {
        this.googleApiKeys = value;
    }
    getGoogleApiKeys(){
        return this.googleApiKeys;
    }
    
}
