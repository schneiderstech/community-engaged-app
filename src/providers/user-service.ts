import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class UserService {

    constructor(
        public http: Http, 
        public storage: Storage
    ) {}

    addUserLocation(details){

        return new Promise((resolve, reject) => {

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');

            this.http.post('http://localhost:8080/api/auth/updateuserlocation', JSON.stringify(details), {headers: headers})
                .subscribe(res => {

                    let data = res.json();

                    this.storage.set('user_location', details.user_location);
                    
                    resolve(data);

                }, (err) => {
                    reject(err);
                });
        });

    }


}
