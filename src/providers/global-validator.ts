import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the GlobalValidator provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class GlobalValidator {
    emailValidation;
    passwordValidation;

    constructor(public http: Http) {
        this.emailValidation = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        this.passwordValidation = '((?=.*[a-z])(?=.*[A-Z]).{6,20})';
        
    }

    setEmailValidation(value) {
        this.emailValidation = value;
    }
    getEmailValidation() {
        return this.emailValidation;
    }

    //  (			# Start of group
    //   (?=.*\d)		#   must contains one digit from 0-9 --> currently disabled
    //   (?=.*[a-z])		#   must contains one lowercase characters
    //   (?=.*[A-Z])		#   must contains one uppercase characters
    //   (?=.*[@#$%])		#   must contains one special symbols in the list "@#$%" --> currently disabled
    //              .		#   match anything with previous condition checking
    //              {6,20}	#   length at least 6 characters and maximum of 20
    // )
    setPasswordValidation(value) {
        this.passwordValidation = value;

    }
    getPasswordValidation() {
        return this.passwordValidation;
    }


}
