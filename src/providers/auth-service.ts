import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
 
@Injectable()
export class AuthService {
 
    public token: any;
 
    constructor(
        public http: Http, 
        public storage: Storage) {
 
    }
 
    checkAuthentication(){

        return new Promise((resolve, reject) => {

            //Load token if exists
            this.storage.get('token').then((value) => {

                this.token = value;

                let headers = new Headers();
                headers.append('Authorization', this.token);

                this.http.get('http://localhost:8080/api/auth/protected', {headers: headers})
                    .subscribe(res => {
                        resolve(res);
                    }, (err) => {
                        reject(err);
                    }); 
            });         
        });

    }
 
    createAccount(details){

        return new Promise((resolve, reject) => {

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');

            this.http.post('http://localhost:8080/api/auth/register', JSON.stringify(details), {headers: headers})
                .subscribe(res => {

                    let data = res.json();

                    this.token = data.token;
                    this.storage.set('token', data.token);

                    // store user email and id, we need to empty both sets once the create account step 2 is finalised via the user.
                    this.storage.set('user_email', data.user.user_email);
                    this.storage.set('user_id', data.user._id);
                    
                    resolve(data);

                }, (err) => {
                    reject(err);
                });
        });

    }
 
    loginUser(credentials){

        return new Promise((resolve, reject) => {
    
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
    
            this.http.post('http://localhost:8080/api/auth/login', JSON.stringify(credentials), {headers: headers})
                .subscribe(res => {

                    let data = res.json();

                    this.token = data.token;
                    this.storage.set('token', data.token);

                    // store user email and id - needs to be removed/deleted after create account step 2 is done!!!!!
                    this.storage.set('user_email', data.user.user_email);
                    this.storage.set('user_id', data.user._id);

                    resolve(data);

                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });    
        });
    }
 
    logoutUser(){
        this.storage.set('token', '');
    }
    
    // TODO: move this to providers/user-service
    getUserCredentials() {

        return this.storage.get('user_email').then((user_email) => {
            return user_email;
        });

    }
}
